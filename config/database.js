const mongoose = require('mongoose');
//definisikan url mongo db
const mongoDB = 'mongodb://localhost/node_project_a';
mongoose.connect(mongoDB, {
    useNewUrlParser: true,
    useCreateIndex: true
});

mongoose.Promise = global.Promise;

module.exports = mongoose;