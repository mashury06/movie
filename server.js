//default code
//include dependency yg telah di install
const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const app = express();

//tambahan
const movies = require('./routes/movies');
const users = require('./routes/users');
const mongoose = require('./config/database');

let jwt = require('jsonwebtoken');

app.set('secretKey', 'nodeRestApi');

//connection
mongoose.connection.on('error', console.error.bind(console, 'MongoDb connection error: '));
//end tambahan
//default code
//menggunakan lib morgan dan bodyparser
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false}));

//routeing default http://locahost:port/
app.get('/', function(req, res) {
    res.json({ "latihan" : "membuat rest api" });
});

//tambahan
//public route
app.use('/users', users);

app.get('/favicon.ico', function (req, res) {
    res.sendStatus(204);
});

//private route
app.use('/movies', validateUser, movies);
//fungsi validasi
function validateUser(req, res, next) {
    jwt.verify(
        req.headers['x-access-token'],
        req.app.get('secretKey'), function(err, decode) {
            if(err) {
                res.json({
                    status: "error", 
                    message: err.message,
                    data: null
                });
            } else {
                //add user id to request
                req.body.userId = decode.id;
                next();
            }
        });
}

//handel request 404 eror
app.use(function(req, res, next) {
    let err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function(err, req, res, next) {
    console.log(err);

    if (err.status === 404)
        res.status(404).json({
            message: "Not found"
        });
    else
        res.status(500).json({
            message: "Something looks wrong :( !!!"
        });
});

//cek status server
app.listen(3000, function() {
    console.log("Server is running");
});