const movieModel = require('../models/movies');

module.exports = {
    //fungsi cari by id
    getById: function(req, res, next) {
        console.log(req.body);

        movieModel.findById(req.params.movieId, function(err, movieInfo) {
            if (err) {
                next(err);
            } else {
                res.json({
                    status: "success",
                    message: "movie Found!",
                    data: {
                        movies: movieInfo
                    }
                });
            }
        });
    },
    //fungsi get all
    getAll: function(req, res, next) {
        if (err) {
            next(err);
        } else {
            
            for(let movie of movies) {
                moviesList.push({
                    id: movie._id,
                    name: movie.name,
                    released_on: movie.released_on
                });
            }

            res.json({
                status: "success",
                message: "Movie List Found!",
                data: {
                    movies: moviesList
                }
            });
        }
    },
    //update data dengan id
    updateById: function(req, res, next) {
        movieModel.findByIdAndUpdate(
            req.params.movieId,
            {name:req.body.name}, function(err, movieInfo) {
                if(err) {
                    next(err);
                } else {
                    res.json({
                        status: "success",
                        message: "Data Updated!!",
                        data: null
                    });
                }
            }
        );
    },
    //delete data by id
    deleteById: function(req, res, next) {
        movieModel.findByIdAndRemove(
            req.params.movieId,
            function(err, movieInfo) {
                if (err) {
                    next(err);
                } else {
                    res.json({
                        status: "success",
                        message: "Data Deleted!!",
                        data: null
                    });
                }
            }
        );
    },
    //insert data
    create: function(req, res, next) {
        movieModel.create({
            name: req.body.name,
            released_on: req.body.released_on
            }, function(err, result) {
                if (err) {
                    next(err);
                } else {
                    res.json({
                        status: "success",
                        message: "Data added!!",
                        data: null
                    });
                }
            }
        );
    },


}